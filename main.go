package main

import (
	"fmt"

	userHandler "backend/api/v1/user/gateway/handler/http"
	userRepository "backend/api/v1/user/repository"
	userUcase "backend/api/v1/user/usecase"

	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("dev")
	viper.SetConfigType("env")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

}
func main() {
	e := echo.New()

	userRepo := userRepository.NewUserRepository()
	userUC := userUcase.NewUserUsecase(&userRepo)
	userHTTPHandler := userHandler.NewUserHandler(&userUC)

	e.Group("api/v1")
	e.GET("/hello", userHTTPHandler.Hello)
	e.Logger.Debug((e.Start(fmt.Sprintf(":%v", viper.GetString("PORT")))))
}
