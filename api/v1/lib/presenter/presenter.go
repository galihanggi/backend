package presenter

type Response struct {
	Message string      `json:"msg"`
	Data    interface{} `json:"data"`
}

type ErrorResponse struct {
	Message string `json:"msg"`
}
