package repository

import userEntity "backend/api/v1/user/domain/entity"

type UserRepositoryContract interface {
	Get(id uint64) (user userEntity.User, err error)
}
type userRepository struct {
}

func NewUserRepository() userRepository {
	return userRepository{}
}

func (r *userRepository) Get(id uint64) (user userEntity.User, err error) {
	for _, v := range USERS {
		if v.ID == id {
			return v, nil
		}
	}
	return
}
