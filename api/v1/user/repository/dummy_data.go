package repository

import (
	userEntity "backend/api/v1/user/domain/entity"
)

var USERS []userEntity.User = []userEntity.User{
	{
		ID:   1,
		Name: "Endy Muhardin",
	},
	{
		ID:   2,
		Name: "Galih Anggi",
	},
}
