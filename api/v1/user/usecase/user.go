package usecase

import (
	userEntity "backend/api/v1/user/domain/entity"
	userRepository "backend/api/v1/user/repository"
)

type UserUsecaseContract interface {
	Get(id uint64) (user userEntity.User, err error)
}

type userUsecase struct {
	userRepo userRepository.UserRepositoryContract
}

func NewUserUsecase(userRepo userRepository.UserRepositoryContract) userUsecase {
	return userUsecase{userRepo}
}
func (uc *userUsecase) Get(id uint64) (user userEntity.User, err error) {
	return uc.userRepo.Get(id)
}
