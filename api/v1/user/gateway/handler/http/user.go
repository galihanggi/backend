package http

import (
	libPresenter "backend/api/v1/lib/presenter"
	userUcase "backend/api/v1/user/usecase"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

type UserHandlerContract interface {
	Hello(e echo.Context) error
}

type userHandler struct {
	userUC userUcase.UserUsecaseContract
}

func NewUserHandler(userUC userUcase.UserUsecaseContract) userHandler {
	return userHandler{userUC}
}

func (h *userHandler) Hello(e echo.Context) error {
	user, err := h.userUC.Get(1)
	if err != nil {
		return e.JSON(http.StatusInternalServerError, libPresenter.ErrorResponse{
			Message: "Internal Server Error",
		})
	}
	resp := libPresenter.Response{
		Message: "Success Get Data",
		Data:    fmt.Sprintf("Hello %v", user.Name),
	}
	return e.JSON(http.StatusOK, resp)
}
