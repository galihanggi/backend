FROM golang:alpine AS build-env
RUN mkdir /go/src/backend && apk update && apk add git
ADD . /go/src/backend
WORKDIR /go/src/backend
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o app .

FROM scratch
WORKDIR /app
COPY --from=build-env /go/src/backend/app .
COPY --from=build-env /go/src/backend/dev.env .
ENTRYPOINT [ "./app" ]